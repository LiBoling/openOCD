# OneOS/openOCD

### 简介
Open On-Chip Debugger (OpenOCD)是一款针对嵌入式系统调试的开源软件，提供调试、烧录、边界扫描等功能。

这里对OpenOCD的改动主要为：

* 增加对OneOS rtos的支持，帮助追溯所有正在运行的线程。

OpenOCD遵循GNU General Public License v2。

### 使用Cygwin在Windows上构建OpenOCD

#### 1.安装Cygwin
*  从[Cygwin官网](https://cygwin.com/)下载setup-x86_64.exe并安装。
*  建议选择"Install from Internet"下载软件包。
*  安装OpenOCD构建所需的软件包。

编译OpenOCD需要的依赖包如下：
- autobuild
- autoconf
- autoconf-archive
- automake
- git
- gcc-core
- libtool
- libusb1.0
- libusb1.0-devel
- make
- pkg-config
- usbutils

#### 2.从源码构建OpenOCD
打开Cygwin终端，克隆代码
```sh
$ git clone https://gitee.com/cmcc-oneos/openOCD.git openocd
Cloning into 'openocd'...
remote: Enumerating objects: 54798, done.
...
```
配置并构建代码
```sh
$ cd openocd
# 这里如果bootstrap是dos文本格式，运行会失败，所以在checkout代码的时候，使用Cygwin的git。
$ ./bootstrap
+ aclocal
+ libtoolize --automake --copy
+ autoconf
+ autoheader
+ automake --gnu --add-missing --copy
...

$ mkdir build
$ cd build
$ ../configure --enable-ftdi --enable-stlink --enable-jlink --disable-werror
...
OpenOCD configuration summary
--------------------------------------------------
MPSSE mode of FTDI based devices        yes
ST-Link JTAG Programmer                 yes
TI ICDI JTAG Programmer                 yes (auto)
Keil ULINK JTAG Programmer              yes (auto)
Altera USB-Blaster II Compatible        yes (auto)
Versaloon-Link JTAG Programmer          yes (auto)
OSBDM (JTAG only) Programmer            yes (auto)
eStick/opendous JTAG Programmer         yes (auto)
Andes JTAG Programmer                   yes (auto)
USBProg JTAG Programmer                 no
Raisonance RLink JTAG Programmer        no
Olimex ARM-JTAG-EW Programmer           no
CMSIS-DAP Compliant Debugger            no
Altera USB-Blaster Compatible           no
ASIX Presto Adapter                     no
OpenJTAG Adapter                        no
SEGGER J-Link Programmer                yes

$ make

$ make install

$ which openocd
/usr/local/bin/openocd
```
运行OpenOCD
```sh
$ openocd.exe -s /usr/local/share/openocd/scripts/ -f "interface\stlink-v2.cfg" -f "target\stm32f4x.cfg"
...
Info : using stlink api v2
Info : Target voltage: 3.259749
Info : stm32f4x.cpu: hardware has 6 breakpoints, 4 watchpoints
```

更多windows下构建及使用相关内容，请参考[README.Windows](README.Windows)。

### 使用说明

请参考
* 原仓库[README](README)。
* [OpenOCD官网](http://openocd.org/) 或文档: [HTML](http://openocd.org/doc/html/index.html) - [PDF](http://openocd.org/doc/pdf/openocd.pdf)。
* [OneOS官方文档](https://os.iot.10086.cn/doc/oneos_cube/debug_with_gdb.html)。

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
